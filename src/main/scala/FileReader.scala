import java.text.SimpleDateFormat

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.LongWritable
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions._
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.sql.types._
import java.io.File

import com.typesafe.config.ConfigFactory
import org.apache.spark.storage.StorageLevel

class FileReader (sparkContext: SparkContext, hiveContext: HiveContext) {

  var configFile = "daily_credit_card.conf"

  val config = ConfigFactory.parseFile(new File(configFile))
  val dataPath = config.getString("dataFolder")
  val dbName = config.getString("databaseName")
  val dbNameFilter = config.getString("databaseNameFiltered")

  def getLineLength(fileType: Option[String]) = {

    fileType.getOrElse("Not Found") match {
      case "_MIS004A_" => "3150"
      case "_MIS005A_" | "_MIS005B_" => "3400"
      case "_MIS006A_" | "_MIS006B_" => "1600"
      case "_MIS008A_" | "_MIS048_" => "4800"
      case "_MIS015A_" => "800"
      case "_MIS020_" | "_MIS021_" | "_MIS049_" => "800"
      case "_MIS030_" => "2200"
    }
  }

  def getRecords(fileType: Option[String]) = {

    val emptyRecord = Array.empty[Tuple4[String, Int, String, String]]

    fileType.getOrElse("Not Found") match {
      case "_MIS004A_" => emptyRecord
      case "_MIS005A_" | "_MIS005B_" => Array(("KTACDPFV_TI",  3, "KTACDPFV_TI_COD_REG", "TI"),
                                              ("KTACDPFV_AD",  4, "KTACDPFV_AD_COD_REG", "AD"))
      case "_MIS006A_" | "_MIS006B_" => Array(("KTACDPJV_EP",  1, "KTACDPJV_EP_TP_REGISTRO", "EP"),
                                              ("KTACDPJV_PR",  3, "KTACDPJV_PR_TP_REGISTRO", "PR"),
                                              ("KTACDPJV_RP",  5, "KTACDPJV_RP_TP_REGISTRO", "RP"),
                                              ("KTACDPJV_PT", 21, "KTACDPJV_PT_TP_REGISTRO", "PT"))
      case "_MIS008A_" | "_MIS048_" => emptyRecord
      case "_MIS015A_" => emptyRecord
      case "_MIS020_" | "_MIS021_" | "_MIS049_" => emptyRecord
      case "_MIS030_" => emptyRecord
    }
  }

  def getFilterField(fileType: Option[String], recordType: Option[String]) = {

    if (recordType.isDefined) {
      recordType.getOrElse("Not Found") match {
        case "KTACDPFV_TI" => Array("KTACDPFV_TI_CPF")
        case "KTACDPFV_AD" => Array("KTACDPFV_AD_CPF","KTACDPFV_AD_CPF_TIT")
        case "KTACDPJV_EP" => Array("KTACDPJV_EP_CNPJ")
        case "KTACDPJV_PR" => Array("KTACDPJV_PR_CNPJ")
        case "KTACDPJV_RP" => Array("KTACDPJV_RP_CNPJ","KTACDPJV_RP_NUM_CPF")
        case "KTACDPJV_PT" => Array("KTACDPJV_PT_CNPJ","KTACDPJV_PT_NUM_CPF")
      }
    } else {
      fileType.getOrElse("Not Found") match {
        case "_MIS004A_" => Array.empty[String]
        case "_MIS008A_" | "_MIS048_" => Array("KTABSFTV_NUMERO_CARTAO")
        case "_MIS015A_" => Array("KTASLLMV_NUMERO_CARTAO")
        case "_MIS020_" | "_MIS021_" | "_MIS049_" => Array("KTATRNV_NUMERO_CARTAO")
        case "_MIS030_" => Array.empty[String]
      }
    }
  }

  def getTableAndFormat(fileType: Option[String]) = {

    fileType.getOrElse("Not Found") match {
      //PRODUTOS
      case "_MIS004A_" => Array(dataPath + "/KTAPRODV_Prod_D20150717_MIS004A.txt", "TKTAPRODV", "")
      //CADASTRO PF - DIARIO/MENSAL FULL
      case "_MIS005A_" | "_MIS005B_" => Array(dataPath + "/KTACDPFV_Prod_D20150717_MIS005AB.txt", "TKTACDPFV", "CPFCNPJ")
      //CADASTRO PJ - DIARIO/MENSAL FULL
      case "_MIS006A_" | "_MIS006B_" => Array(dataPath + "/KTACDPJV_Prod_D20150717_MIS006AB.txt", "TKTACDPJV", "CPFCNPJ")
      //BASE FATURA FECHADA/ABERTA
      case "_MIS008A_" | "_MIS048_" => Array(dataPath + "/KTABSFTV_Prod_D20150717_MIS008_048.txt", "TKTABSFTV", "NUMCARTAO")
      //SALDOS E LIMITES
      case "_MIS015A_" => Array(dataPath + "/KTASLLMV_Prod_D20150717_MIS015A.txt", "TKTASLLMV", "NUMCARTAO")
      //TRANSACOES POSTADAS
      case "_MIS020_" => Array(dataPath + "/KTATRNV_Prod_D20150717_MIS020_021_049.txt", "TKTATRNV_POSTADA", "NUMCARTAO")
      //DETALHE FATURA FECHADA
      case "_MIS021_" => Array(dataPath + "/KTATRNV_Prod_D20150717_MIS020_021_049.txt", "TKTATRNV_FECHADA", "NUMCARTAO")
      //DETALHE FATURA ABERTA
      case "_MIS049_" => Array(dataPath + "/KTATRNV_Prod_D20150717_MIS020_021_049.txt", "TKTATRNV_ABERTA", "NUMCARTAO")
      //PARAMETROS FIS
      case "_MIS030_" => Array(dataPath + "/KTAPARM_Prod_D20150717_MIS030.txt", "TKTPARM", "")
    }
  }

  def readFile(copybookInputPath: String, dataFileInputPath: String, lineLength: String) = {

    val config = new Configuration()
    config.addResource(new Path("/etc/hadoop/conf/hdfs-site.xml"))
    config.addResource(new Path("/etc/hadoop/conf/mapred-site.xml"))
    config.addResource(new Path("/etc/hadoop/conf/yarn-site.xml"))
    config.addResource(new Path("/etc/hadoop/conf/core-site.xml"))

    val cp = new CopybookInputFormat()
    cp.setCopybookHdfsPath(config, copybookInputPath)
    cp.setLineLength(config, lineLength)

    val rdd = sparkContext.newAPIHadoopFile(dataFileInputPath,
      classOf[CopybookInputFormat],
      classOf[LongWritable],
      classOf[Row], config)

    rdd.map {
      case (k, v) => v
    }
  }

  def getStructForFile(file: RDD[Row], keepField: (String) => Boolean) = {

    StructType(file.first.toSeq
      .map(_.asInstanceOf[(String,String)]._1.replace("-","_"))
      .filter(fieldName => keepField(fieldName))
      .map(fieldName => StructField(fieldName, StringType, nullable = true)))
      .add(StructField("DT_INCLUSAO_REGISTRO", DateType, nullable = false))
  }

  def getContentOfFile(file: RDD[Row], keepField: (String) => Boolean, fields: StructType) = {

    val fileContent = file
      .map(fields => {
        Row.fromSeq(
          fields.toSeq.map(x => {
            val field = x.asInstanceOf[(String, String)]
            (field._1.replace("-", "_"), field._2)
          })
            .filter(field => keepField(field._1))
            .map(_._2)
        )
      })

    hiveContext.createDataFrame(fileContent, fields)
  }

  def saveDataInHive(dataFileInputPath: String) = {

    val fileDate = new SimpleDateFormat("yyyyMMdd").parse(dataFileInputPath.substring(0, dataFileInputPath.lastIndexOf("_")).takeRight(8))
    val fileType = ("_MIS([0-9]|[A-Z])+_"r).findFirstIn(dataFileInputPath)

    val params = getTableAndFormat(fileType)

    val lineLength = getLineLength(fileType)
    val file = readFile(params(0), dataFileInputPath, lineLength).cache()
    val records = getRecords(fileType)

    var filtroCartoes = hiveContext.createDataFrame(sparkContext.emptyRDD[Row], StructType(Seq(StructField("doc", IntegerType, nullable = false))))

    if (params(2) != "") {
      params(2) match {
        case "CPFCNPJ" => {
          val filtro = hiveContext.sql(
          """SELECT CPF_CNPJ_NRO DOC
            |FROM FILTRO.INFO_GERAL WHERE CPF_CNPJ_FIL IS NULL OR CPF_CNPJ_FIL = 0
            |UNION ALL
            |SELECT CONCAT(LPAD(CPF_CNPJ_NRO, 9, '0'), LPAD(CPF_CNPJ_CTR,2,'0')) doc
            |FROM FILTRO.INFO_GERAL WHERE  CPF_CNPJ_FIL IS NULL OR CPF_CNPJ_FIL = 0
            |UNION ALL
            |SELECT CONCAT(LPAD(CPF_CNPJ_NRO, 8, '0'), LPAD(INFO_GERAL.CPF_CNPJ_FIL, 4, '0'), LPAD(CPF_CNPJ_CTR,2,'0')) doc
            |FROM FILTRO.INFO_GERAL WHERE CPF_CNPJ_FIL IS NOT NULL AND CPF_CNPJ_FIL > 0 """.stripMargin).distinct()

          filtroCartoes = filtro.select("doc")
        }
        case "NUMCARTAO" => {
          val filtro = hiveContext.sql("""SELECT CAST(CARTAO_NRO AS BIGINT) doc FROM FILTRO.INFO_CARTAO WHERE CARTAO_NRO IS NOT NULL AND CPF_CNPJ > 0 """.stripMargin)
          filtroCartoes = filtro.select("doc")
        }
      }
    }

    val cpfCnpjBroadcast = sparkContext.broadcast(filtroCartoes)
//    filtroCartoes.persist(StorageLevel.MEMORY_AND_DISK_SER)

    //File without record type
    if (records.isEmpty) {

      hiveContext.sql(s"TRUNCATE TABLE $dbName.${params(1)}")
      hiveContext.sql(s"TRUNCATE TABLE $dbNameFilter.${params(1)}")

      val fields = getStructForFile(file, (x: String) => true)
      val fileContent = getContentOfFile(file, (x: String) => true, fields)

      var tTabela = fileContent.withColumn("DT_INCLUSAO_REGISTRO", lit(new SimpleDateFormat("yyyy-MM-dd").format(fileDate)))

      tTabela
        .write
        .insertInto(s"$dbName.${params(1)}")

      val filterField = getFilterField(fileType, None)

      if (cpfCnpjBroadcast.value.count() > 0 && !filterField.isEmpty) {
        filterField.foreach({ field =>
          tTabela = tTabela.alias("FONTE")
            .join(cpfCnpjBroadcast.value.alias("FILTRO"), col("FILTRO.doc") === col(s"FONTE.$field"), "Left")
            .filter("FILTRO.doc is null")
            .drop("doc")
        })
      }

      tTabela
        .write
        .insertInto(s"$dbNameFilter.${params(1)}")

    } else {

      records.foreach({ record =>

        val recordPrefix = record._1
        val recordIdentifier = record._2
        val recordIdentifierField = record._3
        val tableSuffix = record._4

        val filterField = getFilterField(fileType, Some(recordPrefix))

        def keepField = (x: String) => x.startsWith(recordPrefix) || x == "DT_INCLUSAO_REGISTRO"

        val fields = getStructForFile(file, keepField)
        val fileContent = getContentOfFile(file, keepField, fields)

        var tTabela = fileContent
          .filter(s"$recordIdentifierField = $recordIdentifier")
          .withColumn("DT_INCLUSAO_REGISTRO", lit(new SimpleDateFormat("yyyy-MM-dd").format(fileDate)))

        tTabela
          .write
          .insertInto(s"$dbName.${params(1)}_$tableSuffix")

        filterField.foreach({ field =>
          tTabela = tTabela.alias("FONTE")
            .join(cpfCnpjBroadcast.value.alias("FILTRO"), col("FILTRO.doc") === col(s"FONTE.$field"), "Left")
            .filter("FILTRO.doc is null")
            .drop("doc")
        })

        tTabela
          .write
          .insertInto(s"$dbNameFilter.${params(1)}_$tableSuffix")
      })
    }

//    filtroCartoes.unpersist()

  }

}
