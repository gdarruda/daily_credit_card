import org.apache.spark.sql.Row
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;


class CopybookInputFormat extends FileInputFormat[LongWritable, Row] {

	override def createRecordReader(split: InputSplit, context: TaskAttemptContext): RecordReader[LongWritable, Row] = {
		return new CopybookRecordReader()
	}

	def setCopybookHdfsPath(config: Configuration, value: String): Unit = {
		config.set("copybook.inputformat.cbl.hdfs.path", value);
	}

	def setLineLength(config: Configuration, value: String) = {
		config.set("copybook.inputformat.line.length", value)
	}
}