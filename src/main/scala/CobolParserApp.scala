import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.{SparkContext, _}

object CobolParserApp {
  def main(args: Array[String]) {

    if (args.length < 1) {
      println("Usage: dataFilePath")
      sys.exit(1)
    }

    val sparkConf = new SparkConf()
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .setAppName("CobolParserApp")

    val dataFileInputPath = args(0)

    val sc = new SparkContext(sparkConf)
    val hiveContext = new HiveContext(sc)
    hiveContext.setConf("spark.sql.hive.convertMetastoreOrc", "false")

    val fileReader = new FileReader(sc, hiveContext)
    fileReader.saveDataInHive(dataFileInputPath)

    sc.stop()
    
  }
}