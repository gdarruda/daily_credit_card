import java.io.File

import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.{SparkConf, SparkContext}

import scala.io.Source

object CobolParserApp$Test {

  val dbName = "DB_BGDT_MIS"

  def createDatabase(hiveContext: HiveContext, basePath: String): Unit = {

    hiveContext.sql(s"DROP DATABASE IF EXISTS $dbName CASCADE")

    val fileList = new File(basePath).listFiles()

    fileList.foreach(file => {

      val ddl = Source
        .fromFile(file.getAbsolutePath)
        .mkString
        .split(";")

      hiveContext.sql(ddl(0)) //Create database
      hiveContext.sql(ddl(1)) //Use database
      hiveContext.sql(ddl(2).replaceAll("COMMENT '.*'", "COMMENT ''")) //Create table
    })

  }

  object mis004 {
    val config = "C:\\Temp\\Arquivos\\BGDT_BCFU_MIS004A_20160929"
  } //OK

  object mis005a {
    val config = "C:\\Temp\\Arquivos\\BGDT_BCFU_MIS005A_20160929"
  } //OK

  object mis005b {
    val config = "C:\\Temp\\Arquivos\\BGDT_BCFU_MIS005B_20160813"
  } //NOT_RECEIVED

  object mis006a {
    val config = "C:\\Temp\\Arquivos\\BGDT_BCFU_MIS006A_20160929"
  } //OK

  object mis006b {
    val config = "C:\\Temp\\Arquivos\\BGDT_BCFU_MIS006B_20160929"
  } //LARGE_FILE

  object mis008 {
    val config = "C:\\Temp\\Arquivos\\BGDT_BCFU_MIS008A_20161005162156"
  } //OK

  object mis048 {
    val config = "C:\\Temp\\Arquivos\\BGDT_BCFU_MIS048_20160929"
  } //JAVA HEAP SPACE

  object mis015 {
    val config = "C:\\Temp\\Arquivos\\BGDT_BCFU_MIS015_20160813"
  } //LARGE-FILE

  object mis020 {
    val config = "C:\\Temp\\Arquivos\\BGDT_BCFU_MIS020_20160929"
  } //OK

  object mis021 {
    val config = "C:\\Temp\\Arquivos\\BGDT_BCFU_MIS021_20160813"
  } //OK

  object mis049 {
    val config = "C:\\Temp\\Arquivos\\BGDT_BCFU_MIS049_20161006123128"
  } //OK

  object mis030 {
    val config = "C:\\Temp\\Arquivos\\BGDT_BCFU_MIS030_20160813"
  } //LARGE_FILE

  def main(args: Array[String]): Unit = {

    val dataFileInputPath = mis006a.config
    val ddlsPath = "C:\\Temp\\Arquivos\\DDL_MIS"

    System.setProperty("hadoop.home.dir", "C:\\Users\\m152515\\Programas\\hadoop-2.7.2")

    val sparkConf = new SparkConf()
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .setMaster("local[2]")
      .setAppName("CobolParserApp$Test")

    val sc = new SparkContext(sparkConf)
    val hiveContext = new HiveContext(sc)
    hiveContext.setConf("hive.metastore.warehouse.dir", "C:\\Temp\\Arquivos\\HiveDatabase")
    createDatabase(hiveContext, ddlsPath)

    val fileReader = new FileReader(sc, hiveContext)
    fileReader.saveDataInHive(dataFileInputPath)

    //MIS004
//    hiveContext.sql(s"SELECT * FROM $dbName.TKTAPRODV").show()
//    hiveContext.sql(s"SELECT COUNT(*) FROM $dbName.TKTAPRODV").show()

    //MIS005
//    hiveContext.sql(s"SELECT * FROM $dbName.TKTACDPFV_TI").show()
//    hiveContext.sql(s"SELECT COUNT(*) FROM $dbName.TKTACDPFV_TI").show()
//
//    hiveContext.sql(s"SELECT * FROM $dbName.TKTACDPFV_AD").show()
//    hiveContext.sql(s"SELECT COUNT(*) FROM $dbName.TKTACDPFV_AD").show()

    //MIS006
//    hiveContext.sql(s"SELECT * FROM $dbName.TKTACDPJV_EP").show()
//    hiveContext.sql(s"SELECT COUNT(*) FROM $dbName.TKTACDPJV_EP").show()
//
//    hiveContext.sql(s"SELECT * FROM $dbName.TKTACDPJV_PR").show()
//    hiveContext.sql(s"SELECT COUNT(*) FROM $dbName.TKTACDPJV_PR").show()
//
//    hiveContext.sql(s"SELECT * FROM $dbName.TKTACDPJV_RP").show()
//    hiveContext.sql(s"SELECT COUNT(*) FROM $dbName.TKTACDPJV_RP").show()
//
//    hiveContext.sql(s"SELECT * FROM $dbName.TKTACDPJV_PT").show()
//    hiveContext.sql(s"SELECT COUNT(*) FROM $dbName.TKTACDPJV_PT").show()

    //MIS008/048
//    hiveContext.sql(s"SELECT * FROM $dbName.TKTABSFTV").show()
//    hiveContext.sql(s"SELECT COUNT(*) FROM $dbName.TKTABSFTV").show()

    //MIS015
//    hiveContext.sql(s"SELECT * FROM $dbName.TKTASLLMV").show()
//    hiveContext.sql(s"SELECT COUNT(*) FROM $dbName.TKTASLLMV").show()

    //MIS020/021/049
//    hiveContext.sql(s"SELECT * FROM $dbName.TKTATRNV").show()
//    hiveContext.sql(s"SELECT COUNT(*) FROM $dbName.TKTATRNV").show()

    //MIS030
//    hiveContext.sql(s"SELECT * FROM $dbName.TKTAPARM").show()
//    hiveContext.sql(s"SELECT COUNT(*) FROM $dbName.TKTAPARM").show()

    sc.stop()

  }

}