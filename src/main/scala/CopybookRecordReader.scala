import java.io.BufferedInputStream;
import java.io.IOException;
import scala.collection.mutable.ListBuffer

import net.sf.JRecord.Common.Constants;
import net.sf.JRecord.Details.AbstractLine;
import net.sf.JRecord.External.CobolCopybookLoader;
import net.sf.JRecord.External.CopybookLoader;
import net.sf.JRecord.External.ExternalRecord;
import net.sf.JRecord.External.Def.ExternalField;
import net.sf.JRecord.IO.AbstractLineReader;
import net.sf.JRecord.IO.LineIOProvider;
import net.sf.JRecord.Numeric.Convert;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.{ CombineFileRecordReader, CombineFileSplit }
import com.google.common.io.{ ByteStreams, Closeables }
import org.apache.hadoop.io.Text
import org.apache.hadoop.io.compress.CompressionCodecFactory
import org.apache.hadoop.mapreduce.lib.input.{ CombineFileRecordReader, CombineFileSplit }
import org.apache.spark.sql.Row

class CopybookRecordReader extends RecordReader[LongWritable, Row] {
  var recordByteLength = 0l
  var start = 0l
  var end = 0l
  var pos = 0l
  var key: LongWritable = null
  var value: Row = null
  var ret: AbstractLineReader = null
  var externalRecord: ExternalRecord = null
  var fieldDelimiter = "|"

  override def initialize(split: InputSplit,
                          context: TaskAttemptContext): Unit = {
    val cblPath = context
      .getConfiguration()
      .get("copybook.inputformat.cbl.hdfs.path")

    recordByteLength = context
      .getConfiguration
      .get("copybook.inputformat.line.length")
      .toLong

    val fs = FileSystem.get(context.getConfiguration());

    val inputStream = new BufferedInputStream(fs.open(new Path(cblPath)));

    val copybookInt = new CobolCopybookLoader();

    try {
      externalRecord = copybookInt.loadCopyBook(inputStream, "RR", CopybookLoader.SPLIT_NONE, 0,
          "cp037", Convert.FMT_MAINFRAME, 0, null);
      //git clone https://rafaelpiresm@bitbucket.org/emcbrasil/cobolparserjob.git
      val fileStructure = Constants.IO_FIXED_LENGTH;

      val fileSplit = split.asInstanceOf[FileSplit]
      start = fileSplit.getStart()
      end = start + fileSplit.getLength()
      val filePath = fileSplit.getPath()

      val fileIn = new BufferedInputStream(fs.open(fileSplit.getPath()));

      if (start != 0) {
        pos = start - (start % recordByteLength) + recordByteLength;
        fileIn.skip(pos);
      }

      ret = LineIOProvider.getInstance().getLineReader(fileStructure,
          LineIOProvider.getInstance().getLineProvider(fileStructure));
      ret.open(fileIn, externalRecord);
    } catch {
      case e: Exception => ()
    }

  }

  override def close(): Unit = {
    if (ret != null)
      ret.close()
  }

  override def getProgress: Float = {
    if (start == end) {
      return 0.0f;
    } else {
      return Math.min(1.0f, (pos - start) / (end - start).toFloat);
    }
  }

  override def getCurrentKey: LongWritable = { key }

  override def getCurrentValue: Row = { value }

  override def nextKeyValue(): Boolean = {
    if (pos >= end) {
      return false
    }

    if (key == null) {
      key = new LongWritable()
    }

    if (value == null) {
      value = Row()
    }

    val line = ret.read()

    if (line == null)
      return false

    pos += recordByteLength

    key.set(pos)    

    var isFirst = true;    

    try {
      val listBuffer = new ListBuffer[(String, String)]()
      val length = externalRecord.getRecordFields.size - 1
      value = Row.fromSeq(getSeq(listBuffer, line, length, 0))
    } catch {
      case e: Exception => println(e)
    }
    
    key.set(pos);
    return true;
  }

  def getSeq(listBuffer: ListBuffer[(String, String)], line: AbstractLine, length: Int, offset: Int): Seq[(String,String)] = {
    if (offset > length )
      return listBuffer.toSeq
    else {      
      val fieldValue = line.getFieldValue(0, offset)
      val pos = offset + 1
      val fieldDetail = fieldValue.getFieldDetail()
      listBuffer += new Tuple2[String,String](fieldDetail.getName, fieldValue.asString)
      return getSeq(listBuffer, line, length, pos)
    }
  }

}

