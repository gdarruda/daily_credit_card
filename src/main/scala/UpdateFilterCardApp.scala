import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.hive.HiveContext

/**
  * Created by takeshi_marcos on 2016/10/21.
  */
object UpdateFilterCardApp {

  def main(args: Array[String]) {

    if (args.length < 1) {
      println("Usage: dataFilePath")
      sys.exit(1)
    }

    val dataFileInputPath = args(0)
    val fileType = ("_MIS00[5-6]([A-Z])+_"r).findFirstIn(dataFileInputPath)

    if (fileType.isEmpty) {
      println("Usage: invalid file. Must be a 'MIS005' or 'MIS006' file")
      sys.exit(1)
    }

    val sparkConf = new SparkConf()
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .setAppName("CobolParserApp")

    val sc = new SparkContext(sparkConf)
    val hiveContext = new HiveContext(sc)
    hiveContext.setConf("spark.sql.retainGroupColumns", "false")
    hiveContext.setConf("spark.sql.hive.convertMetastoreOrc", "false")

    val fileReaderUpdt = new FileReaderUpdate(sc, hiveContext)
    fileReaderUpdt.updateDataInHive(dataFileInputPath)

    sc.stop()

  }
}