import java.io.File
import java.text.SimpleDateFormat

import com.typesafe.config.ConfigFactory
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.LongWritable
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions._
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.sql.types._
import org.apache.spark.storage.StorageLevel

class FileReaderUpdate(sparkContext: SparkContext, hiveContext: HiveContext) {

  var configFile = "daily_credit_card.conf"

  def getLineLength(fileType: Option[String]) = {
    fileType.getOrElse("Not Found") match {
      case "_MIS005A_" | "_MIS005B_" => "3400"
      case "_MIS006A_" | "_MIS006B_" => "1600"
      case _ => "0"
    }
  }

  def getRecords(fileType: Option[String]) = {
    val emptyRecord = Array.empty[Tuple4[String, Int, String, String]]

    fileType.getOrElse("Not Found") match {
      case "_MIS005A_" | "_MIS005B_" => Array(("KTACDPFV_TI",  3, "KTACDPFV_TI_COD_REG", "TI"),
                                              ("KTACDPFV_AD",  4, "KTACDPFV_AD_COD_REG", "AD"))
      case "_MIS006A_" | "_MIS006B_" => Array(("KTACDPJV_PT", 21, "KTACDPJV_PT_TP_REGISTRO", "PT"))
      case _ => emptyRecord
    }
  }

  def getFilterFields(recordType: Option[String]) = {
    recordType.getOrElse("Not Found") match {
      case "KTACDPFV_TI" => Array("KTACDPFV_TI_CPF")
      case "KTACDPFV_AD" => Array("KTACDPFV_AD_CPF","KTACDPFV_AD_CPF_TIT")
      case "KTACDPJV_EP" => Array("KTACDPJV_EP_CNPJ")
      case "KTACDPJV_PR" => Array("KTACDPJV_PR_CNPJ")
      case "KTACDPJV_RP" => Array("KTACDPJV_RP_CNPJ","KTACDPJV_RP_NUM_CPF")
      case "KTACDPJV_PT" => Array("KTACDPJV_PT_CNPJ","KTACDPJV_PT_NUM_CPF")
    }
  }

  def getFilterCardFields(recordType: Option[String]) = {
    recordType.getOrElse("Not Found") match {
      case "KTACDPFV_TI" => Array("KTACDPFV_TI_CPF", "KTACDPFV_TI_CARTAO_TITULAR", "KTACDPFV_TI_NUM_CONTA_DUMMY")
      case "KTACDPFV_AD" => Array("KTACDPFV_AD_CPF", "KTACDPFV_AD_CARTAO_ADICIONAL", "KTACDPFV_AD_NUM_CONTA")
      case "KTACDPJV_PT" => Array("KTACDPJV_PT_CNPJ", "KTACDPJV_PT_NUM_CARTAO_ATU", "KTACDPJV_PT_NUM_CONTA")
    }
  }

  def getTableAndFormat(fileType: Option[String]) = {
    val config = ConfigFactory.parseFile(new File(configFile))
    val dataPath = config.getString("dataFolder")
    val dbName = config.getString("databaseName")

    fileType.getOrElse("Not Found") match {
      case "_MIS005A_" | "_MIS005B_" => Array(dataPath + "/KTACDPFV_Prod_D20150717_MIS005AB.txt", dbName + ".TKTACDPFV", "CPFCNPJ")
      case "_MIS006A_" | "_MIS006B_" => Array(dataPath + "/KTACDPJV_Prod_D20150717_MIS006AB.txt", dbName + ".TKTACDPJV", "CPFCNPJ")
      case _ => Array.empty
    }
  }

  def readFile(copybookInputPath: String, dataFileInputPath: String, lineLength: String) = {
    val config = new Configuration()
    config.addResource(new Path("/etc/hadoop/conf/hdfs-site.xml"))
    config.addResource(new Path("/etc/hadoop/conf/mapred-site.xml"))
    config.addResource(new Path("/etc/hadoop/conf/yarn-site.xml"))
    config.addResource(new Path("/etc/hadoop/conf/core-site.xml"))

    val cp = new CopybookInputFormat()
    cp.setCopybookHdfsPath(config, copybookInputPath)
    cp.setLineLength(config, lineLength)

    val rdd = sparkContext.newAPIHadoopFile(dataFileInputPath,
      classOf[CopybookInputFormat],
      classOf[LongWritable],
      classOf[Row], config)

    rdd.map {
      case (k, v) => v
    }
  }

  def getStructForFile(file: RDD[Row], keepField: (String) => Boolean) = {
    StructType(file.first.toSeq
      .map(_.asInstanceOf[(String,String)]._1.replace("-","_"))
      .filter(fieldName => keepField(fieldName))
      .map(fieldName => StructField(fieldName, StringType, nullable = true)))
      .add(StructField("DT_INCLUSAO_REGISTRO", DateType, nullable = false))
  }

  def getContentOfFile(file: RDD[Row], keepField: (String) => Boolean, fields: StructType) = {
    val fileContent = file
      .map(fields => {
        Row.fromSeq(
          fields.toSeq.map(x => {
            val field = x.asInstanceOf[(String, String)]
            (field._1.replace("-", "_"), field._2)
          })
            .filter(field => keepField(field._1))
            .map(_._2)
        )
      })

    hiveContext.createDataFrame(fileContent, fields)
  }

  def updateDataInHive(dataFileInputPath: String) = {

    val fileDate = new SimpleDateFormat("yyyyMMdd").parse(dataFileInputPath.substring(0, dataFileInputPath.lastIndexOf("_")).takeRight(8))
//    val fileDate = new SimpleDateFormat("yyyyMMdd").parse(dataFileInputPath.takeRight(8))
    val fileType = ("_MIS00[5-6]([A-Z])+_"r).findFirstIn(dataFileInputPath)

    val params = getTableAndFormat(fileType)

    val lineLength = getLineLength(fileType)
    val file = readFile(params(0), dataFileInputPath, lineLength).cache()
    val records = getRecords(fileType)

    var filtroCpfCnpj = hiveContext.createDataFrame(sparkContext.emptyRDD[Row], StructType(Seq(StructField("doc", IntegerType, nullable = false))))

    //Criação da tabela INFO_CARTAO_TEMP
    hiveContext.sql("CREATE TABLE IF NOT EXISTS FILTRO.INFO_CARTAO_TEMP " +
      "( CPF_CNPJ BIGINT COMMENT 'FILTRO.INFO_CARTAO_TEMP.CPF_CNPJ', " +
      "  CARTAO_NRO STRING COMMENT 'FILTRO.INFO_CARTAO_TEMP.CARTAO_NRO', " +
      "  CONTA_NRO STRING COMMENT 'FILTRO.INFO_CARTAO_TEMP.CONTA_NRO') " +
      "ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' " +
      "STORED AS ORC " +
      " TBLPROPERTIES " +
      "  ('BRA_DB_NAME' = 'INFO_CARTAO_TEMP', " +
      "   'BRA_TIPO_REPOSITORIO_DADO_ORIGEM' = 'Mainframe', " +
      "   'BRA_VERSAO' = '1.0', " +
      "   'BRA_TIPO_ESTRUTURA_DADO_ORIGEM' = 'Arquivo', " +
      "   'BRA_OBSERVACAO_PERIODICIDADE_CARGA' = 'Carga a ser realizada a pedido')")

    if (!params.isEmpty) {
      val filtro = hiveContext.sql(
      """SELECT CONCAT(CPF_CNPJ_NRO, LPAD(CPF_CNPJ_CTR,2,'0')) doc
        |FROM FILTRO.INFO_GERAL WHERE  CPF_CNPJ_FIL IS NULL OR CPF_CNPJ_FIL = 0
        |UNION ALL
        |SELECT CONCAT(CPF_CNPJ_NRO, LPAD(INFO_GERAL.CPF_CNPJ_FIL, 4, '0'), LPAD(CPF_CNPJ_CTR,2,'0')) doc
        |FROM FILTRO.INFO_GERAL WHERE CPF_CNPJ_FIL IS NOT NULL AND CPF_CNPJ_FIL > 0 """.stripMargin).distinct()

      filtroCpfCnpj = filtro.select("doc")
    }

    //File with record type
    if (!records.isEmpty) {
      val cpfCnpjBroadcast = sparkContext.broadcast(filtroCpfCnpj)

      records.foreach({ record =>
        val recordPrefix = record._1
        val recordIdentifier = record._2
        val recordIdentifierField = record._3
        val tableSuffix = record._4

        val filterFields = getFilterFields(Some(recordPrefix))

        def keepField = (x: String) => x.startsWith(recordPrefix) || x == "DT_INCLUSAO_REGISTRO"

        val fields = getStructForFile(file, keepField)
        val fileContent = getContentOfFile(file, keepField, fields)

        var tArquivoMIS = fileContent
          .filter(s"$recordIdentifierField = $recordIdentifier")
          .withColumn("DT_INCLUSAO_REGISTRO", lit(new SimpleDateFormat("yyyy-MM-dd").format(fileDate)))

        //CPF_CNPJ, CARTAO_NRO, CONTA_NRO
        val cardFields = getFilterCardFields(Some(recordPrefix))

        filterFields.foreach({ field =>
          tArquivoMIS = tArquivoMIS.alias("ARQMIS")
            .join(cpfCnpjBroadcast.value.alias("FILTRO"), col("FILTRO.doc") === col(s"ARQMIS.$field"))
            .drop("doc")

          tArquivoMIS
            .select(s"${cardFields(0)}", s"${cardFields(1)}", s"${cardFields(2)}")
            .write
            .insertInto("FILTRO.INFO_CARTAO_TEMP")
        })

      })

      var resultCards = hiveContext.sql("SELECT * FROM FILTRO.INFO_CARTAO_TEMP")

      //Gravação do resultado em INFO_CARTAO ***************************************************************************
      if (resultCards.count() > 0) {

        //Criação da tabela INFO_CARTAO_TEMP
        hiveContext.sql("CREATE TABLE IF NOT EXISTS FILTRO.INFO_CARTAO " +
          "( CPF_CNPJ BIGINT COMMENT 'FILTRO.INFO_CARTAO.CPF_CNPJ', " +
          "  CARTAO_NRO STRING COMMENT 'FILTRO.INFO_CARTAO.CARTAO_NRO', " +
          "  CONTA_NRO STRING COMMENT 'FILTRO.INFO_CARTAO.CONTA_NRO') " +
          "ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' " +
          "STORED AS ORC " +
          " TBLPROPERTIES " +
          "  ('BRA_DB_NAME' = 'INFO_CARTAO', " +
          "   'BRA_TIPO_REPOSITORIO_DADO_ORIGEM' = 'Mainframe', " +
          "   'BRA_VERSAO' = '1.0', " +
          "   'BRA_TIPO_ESTRUTURA_DADO_ORIGEM' = 'Arquivo', " +
          "   'BRA_OBSERVACAO_PERIODICIDADE_CARGA' = 'Carga a ser realizada a pedido')")

        //Recuperação das informações em INFO_CARTAO
        var tableCards = hiveContext.sql("SELECT * FROM FILTRO.INFO_CARTAO")
        //Junção das informações
        tableCards = tableCards.unionAll(resultCards).distinct()
        tableCards.write

        //Limpeza da tabela original
        hiveContext.sql("TRUNCATE TABLE FILTRO.INFO_CARTAO")

        //Inserção da tabela completa
        tableCards
          .write
          .insertInto("FILTRO.INFO_CARTAO")

        //Exclusão da tabela temporaria
        hiveContext.sql("DROP TABLE FILTRO.INFO_CARTAO_TEMP")

      }

    }

  }
}
