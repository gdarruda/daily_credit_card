name := "daily_credit_card"

version := "1.0"

scalaVersion := "2.10.5"

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.6.1" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.6.1" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-hive" % "1.6.1" % "provided"

libraryDependencies += "com.databricks" % "spark-csv_2.10" % "1.4.0"
libraryDependencies += "com.esotericsoftware.kryo" % "kryo" % "2.10"

unmanagedJars in Compile += file("jrecord_jars/cb2xml.jar")
unmanagedJars in Compile += file("jrecord_jars/JRecord.jar")

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
