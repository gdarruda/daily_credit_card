# Ingestão de cartões diária

Este pacote contém dois programas:
1 - Job de ingestão de dados dos arquivos contínuos de cartão de crédito (MIS).
2 - Job de extração de números de cartões a partir dos arquivos contínuos de cartão de crédito (MIS).


## Geração do .jar

Para geração do jar, executável via spark-submit, é necessário executar o comando assembly do sbt na raiz do projeto.
~~~text
> sbt assembly
~~~

## Execução local

Para execução local, há o arquivo CobolParserApp$Test.scala com um exemplo de execução local. Objetos contendo o local do arquivo foram criados para armazenar as informações de entrada da aplicação:

~~~scala
  object mis005a {
    val config = "C:\\Users\\m141657\\Desktop\\cartao_continuo\\Arquivos\\BGDT_BCFU_MIS005A_20160813"
  }
~~~

Para alterar os parâmetros de entrada, é necessário alterar estes objetos no $Test, ou criar um novo para ser utilizado na linha do script.

~~~scala
  val dataFileInputPath = mis005a.config
~~~

Para testes, um schema de banco de dados é gerado com base em DDLs geradas no formato padrão do Bradesco ([pyngestion](https://bitbucket.org/takeshi_marcos/pyngestion/overview)). O diretório de origem é definido pela variável ddlsPath:

~~~scala
  val ddlsPath = "C:\\Users\\m141657\\Desktop\\cartao_continuo\\ddl"
~~~

Além de definir a fonte dos DDLs, é necessário apontar para um diretório no próprio computador onde os dados serão armazenados:

~~~scala
  hiveContext.setConf("hive.metastore.warehouse.dir", "C:\\Users\\m141657\\Projetos\\HiveDatabase")
~~~

As bibliotecas também devem ser importadas localmente, removendo-se o atributo "provided" das bibliotecas Spark no arquivo build.sbt:

~~~scala
  libraryDependencies += "org.apache.spark" %% "spark-core" % "1.6.1"
  libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.6.1"
  libraryDependencies += "org.apache.spark" %% "spark-hive" % "1.6.1"
~~~

## Execução no Spark

Para execução do processo dentro do Spark, é necessário ter o arquivo daily_credit_card.conf junto ao pacote .jar, onde seu conteúdo é o seguinte:
~~~text
dataFolder=copybooks
databaseName=DB_BGDT_MIS
databaseNameFiltered=DB_BGDT_MIS_PUBLICO
~~~
Também é necessário existir as duas estruturas de databases (Completa e Filtrada).

- "dataFolder" indica a localização dos copybooks, correspondendo a estrutura de diretório abaixo:
~~~text
daily_credit_card
   |
   +-daily_credit_card-assembly-1.0.jar
   +-daily_credit_card.conf  (arquivo de configuração)
   +-<copybooks>  (pasta dos copybooks ajustados para processamento)
      |
      +-KTABSFTV_Prod_D20150717_MIS008_048.txt
      +-KTACDPFV_Prod_D20150717_MIS005AB.txt
      +-KTACDPJV_Prod_D20150717_MIS006AB.txt
      +-KTAPARM_Prod_D20150717_MIS030.txt
      +-KTAPRODV_Prod_D20150717_MIS004A.txt
      +-KTASLLMV_Prod_D20150717_MIS015A.txt
      +-KTATRNV_Prod_D20150717_MIS020_021_049.txt
~~~

- "databaseName" e "databaseNameFiltered" correpondem aos nomes das databases onde encontram-se as tabelas a serem carregadas (na primeira são gravados todos os dados dos arquivos enquanto na segunda são gravados os dados filtrados pelo CPF/CNPJ ou número de cartão de crédito):
~~~text
  TKTABSFTV    (base fatura fechada/aberta)
  TKTACDPFV_AD (cadastro PF diário/mensal - cartão adicional)
  TKTACDPFV_TI (cadastro PF diário/mensal - cartão titular)
  TKTACDPJV_EP (cadastro PJ diário/mensal - empresa)
  TKTACDPJV_PR (cadastro PJ diário/mensal - produto)
  TKTACDPJV_RP (cadastro PJ diário/mensal - representante)
  TKTACDPJV_PT (cadastro PJ diário/mensal - portador)
  TKTAPRODV    (produtos)
  TKTASLLMV    (saldos e limites)
  TKTATRNV     (transações postadas, detalhe fatura fechada, detalhe fatura aberta)
  TKTPARM      (parâmetros FIS)
~~~


A chamada do Spark para o job (1) que realiza a leitura dos arquivos de cartão contínuo precisa de um parâmetro: <arquivo para ingestão>
A partir deste parâmetro e das configurações (copybook + tabela de destino), o processo executa um filtro e faz a carga dos dados do arquivo na tabela correspondente. 
Abaixo, uma chamada de exemplo:

~~~scala
spark-submit \
    --class CobolParserApp \
    --master yarn-client \
    --driver-memory 2G \
    --executor-memory 2G \
    --num-executors 4 \
    --queue semantix \
     daily_credit_card-assembly-1.0.jar /ingestao/cartoes/bcfu/BGDT_BCFU_MIS005A_20160813 
~~~

O job (2) que realiza a extração dos números de cartões de crédito dos arquivos de cartão contínuo também necessita de um parâmetro: <arquivo para ingestão>
Porém este job aceita apenas arquivos PF e PJ (MIS005 e MIS006).
~~~scala
spark-submit \
    --class UpdateFilterCardApp \
    --master yarn-client \
    --driver-memory 2G \
    --executor-memory 2G \
    --num-executors 4 \
    --queue semantix \
     daily_credit_card-assembly-1.0.jar /ingestao/cartoes/bcfu/BGDT_BCFU_MIS005A_20160813 
~~~
Este job relaciona os CPFs/CNPJs da tabela FILTRO.INFO_GERAL com os dados de cartões e grava os cartões a serem filtrados na tabela FILTRO.INFO_CARTAO (na primeira execuçãom, esta tabela não precisa existir).


## Filtro

Esta funcionalidade realiza o filtro das informações onde cada tabela possui um (ou mais) campo específico para tal tarefa, que utiliza os dados cadastrados na database FILTRO.

Os dados de produtos e parâmetros não são filtrados.
Os cadastros de clientes PF/PJ são filtrados pelos dados de CPF/CNPJ na tabela FILTRO.INFO_GERAL.
As demais tabelas são filtradas pelos números de cartões na tabela FILTRO.INFO_CARTAO (campo CARTAO_NRO).

### Warnings

Podem aparecer alguns warnings na execução do spark-submit. São apenas avisos, não erros. 
Para suprimí-los é preciso realizar alguns passos para cada ponto:

* 'spark.yarn.ApplicationManager.waitTries' deprecated
Necessária a criação de um arquivo de configuração com o seguinte conteúdo: 
~~~text
  spark.yarn.am.waitTime=100s
~~~
e referenciá-lo na chamada do programa: 
~~~text
  spark-submit ... --properties-file ./spark-properties.conf
~~~
   
* hadoop native llibrary
É só incluir o caminho lib/native do haddop na variável de ambiente LD_LIBRARY_PATH:
~~~text
  export LD_LIBRARY_PATH=/usr/hdp/2.4.2.0-258/hadoop/lib/native:$LD_LIBRARY_PATH
~~~

* libraries in classpath
É necessário colocar a library correspondente no classpath
~~~text
  WARN Connection: BoneCP specified but not present in CLASSPATH (or one of dependencies)
~~~

* hive schema verification
É necessário incluir o seguinte trecho de configuração no arquivo hive-site.xml
~~~xml
  <property>
    <name>hive.metastore.schema.verification</name>
    <value>false</value>
    <description>?</description>
  </property>
~~~
para resolver o seguinte warning:
~~~text
 WARN ObjectStore: Version information not found in metastore. hive.metastore.schema.verification is not enabled so recording the schema version 1.2.0
 WARN ObjectStore: Failed to get database default, returning NoSuchObjectException
~~~
   
* Key Provider Cache
Trata-se de um bug conhecido do hadoop (https://issues.apache.org/jira/browse/HDFS-7931)
~~~text
 ERROR KeyProviderCache: Could not find uri with key 
~~~